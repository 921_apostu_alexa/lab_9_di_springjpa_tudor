package repository;

import model.Client;
import model.exceptions.BaseException;
import model.exceptions.ValidatorException;
import model.validators.Validator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ClientFileRepository extends InMemoryRepository<Long, Client> {
    private String filename;

    public ClientFileRepository(Validator<Client> validator, String filename) {
        super(validator);
        this.filename = filename;

        this.loadFromFile();
    }

    private String toCSV(Client client) {
        return "" + client.getName() + "," + client.getEmail();
    }

    private void loadFromFile() {
        Path path = Paths.get(this.filename);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                String name = items.get(0);
                String email = items.get(1);

                Client client = new Client(name, email);

                try {
                    super.save(client);
                } catch (BaseException be) {
                    be.printStackTrace();
                }
            });
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void saveToFile(Client client) {
        Path path = Paths.get(this.filename);

        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            System.out.println(client);
            bufferedWriter.write(this.toCSV(client));
            bufferedWriter.newLine();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void saveAllToFile() {
        Path path = Paths.get(this.filename);

        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.WRITE)) {
            super.findAll().forEach(client -> {
                try {
                    System.out.println(client);
                    bufferedWriter.write(this.toCSV(client));
                    bufferedWriter.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public Optional<Client> save(Client client) throws ValidatorException {
        Optional<Client> optional = super.save(client);
        if (optional.isPresent())
            return Optional.empty();

        this.saveToFile(client);
        return optional;
    }

    @Override
    public Optional<Client> delete(Long id) {
        Optional<Client> optional = super.delete(id);
        if (optional.isPresent())
            return Optional.empty();

        this.saveAllToFile();
        return optional;
    }

    @Override
    public Optional<Client> update(Client client) throws ValidatorException {
        Optional<Client> optional = super.update(client);
        if (optional.isPresent())
            return Optional.empty();

        this.saveAllToFile();
        return optional;
    }
}
