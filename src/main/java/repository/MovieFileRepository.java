package repository;

import model.Movie;
import model.exceptions.BaseException;
import model.exceptions.ValidatorException;
import model.validators.Validator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MovieFileRepository extends InMemoryRepository<Long, Movie> {
    private String filename;

    public MovieFileRepository(Validator<Movie> validator, String filename) {
        super(validator);
        this.filename = filename;

        this.loadFromFile();
    }

    private String toCSV(Movie movie) {
        return "" + movie.getTitle() + "," + movie.getDuration() + "," + movie.getYear();
    }

    private void loadFromFile() {
        Path path = Paths.get(this.filename);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                String title = items.get(0);
                int minutes = Integer.parseInt(items.get(1));
                int year = Integer.parseInt(items.get(2));

                Movie movie = new Movie(title, minutes, year);

                try {
                    super.save(movie);
                } catch (BaseException be) {
                    be.printStackTrace();
                }
            });
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void saveToFile(Movie movie) {
        Path path = Paths.get(this.filename);

        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(this.toCSV(movie));
            bufferedWriter.newLine();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void saveAllToFile() {
        Path path = Paths.get(this.filename);

        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.WRITE)) {
            super.findAll().forEach(movie -> {
                try {
                    bufferedWriter.write(this.toCSV(movie));
                    bufferedWriter.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public Optional<Movie> save(Movie movie) throws ValidatorException {
        Optional<Movie> optional = super.save(movie);
        if (optional.isPresent())
            return Optional.empty();

        this.saveToFile(movie);
        return optional;
    }

    @Override
    public Optional<Movie> delete(Long id) {
        Optional<Movie> optional = super.delete(id);
        if (optional.isPresent())
            return Optional.empty();

        this.saveAllToFile();
        return optional;
    }

    @Override
    public Optional<Movie> update(Movie movie) throws ValidatorException {
        Optional<Movie> optional = super.update(movie);
        if (optional.isPresent()) {
            this.saveAllToFile();
            return optional;
        }
        return Optional.empty();
    }

}
