package ui;

import controller.ClientController;
import controller.MovieController;
import controller.RentalController;
import model.Client;
import model.Movie;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Console {
    private ClientController clientController;
    private MovieController movieController;
    private RentalController rentalController;
    private Menu menu;
    private boolean running = true;

    public Console(ClientController clientController, MovieController movieController, RentalController rentalController) {
        this.clientController = clientController;
        this.movieController = movieController;
        this.rentalController = rentalController;
        this.initialiseMenu();
    }

    public void initialiseMenu() {
        this.menu = new Menu();
        this.menu.addCommand("c1", "Add client", this::addClient);
        this.menu.addCommand("c2", "Print all clients", this::printClients);
        this.menu.addCommand("c3", "Filter clients by name", this::filterClients);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("m1", "Add movie", this::addMovie);;
        this.menu.addCommand("m2", "Print all movies", this::printMovies);
        this.menu.addCommand("m3", "Filter movies by year", this::filterMovies);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("r1", "Add rental", this::rentMovie);
        this.menu.addCommand("r2", "Print all rentals", this::printRentals);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("x", "Exit",() -> {this.running = false;} );
    }

    public void run() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(this.menu.getMenu());
        try {
            System.out.println("\n>");
            String choice = buffer.readLine();
            this.menu.run(choice);

            if (this.running)
                this.run();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }


    private Client readClient() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            System.out.println("Enter client email: ");
            String email = buffer.readLine();

            return new Client(name, email);

        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
        return null;
    }

    private Movie readMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter movie title: ");
            String title = buffer.readLine();

            System.out.println("Enter movie duration (in minutes): ");
            int duration = Integer.parseInt(buffer.readLine());

            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            return new Movie(title, duration, year);

        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
        return null;
    }

    public void addClient() {
        System.out.println(" -- Adding client -- ");
        Client client = readClient();
        this.clientController.addClient(client);
    }

    public void printClients() {
        System.out.println(" << Clients >> ");
        this.clientController.getClients().stream().sorted()
                .forEach(System.out::println);
        System.out.println();
    }

    public void filterClients() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Filtering clients -- ");
            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            this.clientController.filterClientsByName(name).forEach(System.out::println);

        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void addMovie() {
        Movie movie = readMovie();
        this.movieController.addMovie(movie);
    }
    public void printMovies() {
        System.out.println(" << Movies >> ");
        this.movieController.getMovies().stream().sorted()
                .forEach(System.out::println);
        System.out.println();
    }

    public void filterMovies() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Filtering movies -- ");
            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            this.movieController.filterMoviesByYear(year).forEach(System.out::println);

        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void rentMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter client email: ");
            String clientId = buffer.readLine();
            long cId = Long.parseLong(clientId);
            Client client = this.clientController.getClient(cId);

            System.out.println("Enter movie id: ");
            Long movieId = Long.parseLong(buffer.readLine());
            Movie movie = this.movieController.getMovie(movieId);

            this.rentalController.rent(client, movie);

        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }
    public void printRentals() {
        this.rentalController.getRentals().forEach(System.out::println);
    }
}

