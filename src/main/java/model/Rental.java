package model;

import java.util.Date;

public class Rental extends BaseClass<Long> {
    private Client client;
    private Movie movie;
    private Date rentDate;  // for 2 weeks
    private static long rentId = 0;

    public Rental(Client client, Movie movie, Date rentDate) {
        this.client = client;
        this.movie = movie;
        this.rentDate = rentDate;
        this.setId(++rentId);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Date getRentDate() {
        return rentDate;
    }

    public void setRentDate(Date rentDate) {
        this.rentDate = rentDate;
    }

    @Override
    public String toString() {
        return getId() + ": " + getClient().getName() + " rented " + getMovie().getTitle() + " on " + getRentDate().toString();

    }
}
