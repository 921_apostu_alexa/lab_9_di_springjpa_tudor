import controller.ClientController;
import controller.MovieController;
import controller.RentalController;
import model.Client;
import model.Movie;
import model.Rental;
import model.exceptions.BaseException;
import model.validators.ClientValidator;
import model.validators.MovieValidator;
import model.validators.RentalValidator;
import model.validators.Validator;
import repository.ClientFileRepository;
import repository.IRepository;
import repository.InMemoryRepository;
import repository.MovieFileRepository;
import ui.Console;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Validator<Client>   clientValidator = new ClientValidator();
        Validator<Movie>    movieValidator  = new MovieValidator();
        Validator<Rental>   rentalValidator = new RentalValidator();

        IRepository<Long, Client> clientRepo = new ClientFileRepository(clientValidator, "src/main/resources/clients.txt");
        IRepository<Long, Movie> movieRepo = new MovieFileRepository(movieValidator, "src/main/resources/movies.txt");
        IRepository<Long, Rental> rentalRepo = new InMemoryRepository<>(rentalValidator);

        Client c1 = clientRepo.findOne(Long.parseLong("1")).get();
        Client c2 = clientRepo.findOne(Long.parseLong("2")).get();

        Movie m1 = movieRepo.findOne(Long.parseLong("1")).get();
        Movie m2 = movieRepo.findOne(Long.parseLong("2")).get();

        rentalRepo.save(new Rental(c1, m1, new Date()));
        rentalRepo.save(new Rental(c1, m2, new Date()));
        rentalRepo.save(new Rental(c2, m1, new Date()));


        ClientController clientCtrl = new ClientController(clientRepo);
        MovieController movieCtrl = new MovieController(movieRepo);
        RentalController rentalCtrl = new RentalController(rentalRepo);

        Console cons = new Console(clientCtrl, movieCtrl, rentalCtrl);
        try {
            cons.run();
        } catch (BaseException be) {
            System.out.println(be.getMessage());
        }
    }
}
