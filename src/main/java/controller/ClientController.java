package controller;

import model.Client;
import model.exceptions.BaseException;
import model.exceptions.ValidatorException;
import repository.IRepository;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ClientController {
    private IRepository<Long, Client> repo;

    public ClientController(IRepository<Long, Client> repo) {
        this.repo = repo;
    }

    public void addClient(Client client) throws ValidatorException {
        this.repo.save(client);
    }

    public Client getClient(Long ID) throws BaseException {
        Optional<Client> clientOptional = this.repo.findOne(ID);
        return clientOptional.orElseThrow(() -> new BaseException("No item with given id"));
    }

    public void updateClient(Client client) {
        this.repo.update(client);
    }

    public void removeClient(Long ID) {
        this.repo.delete(ID);
    }

    public Set<Client> getClients() {
        return StreamSupport.stream(this.repo.findAll().spliterator(), false)
                            .collect(Collectors.toSet());
    }

    public Set<Client> filterClientsByName(String name) {
        return StreamSupport.stream(this.repo.findAll().spliterator(), false)
                            .filter(client -> client.getName().contains(name))
                            .collect(Collectors.toSet());
    }
}
