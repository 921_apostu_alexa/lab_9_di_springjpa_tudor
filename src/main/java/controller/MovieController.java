package controller;

import model.Movie;
import model.exceptions.BaseException;
import model.exceptions.ValidatorException;
import repository.IRepository;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MovieController {
    private IRepository<Long, Movie> repo;

    public MovieController(IRepository<Long, Movie> repo) {
        this.repo = repo;
    }

    public void addMovie(Movie movie) throws ValidatorException {
        this.repo.save(movie);
    }

    public Movie getMovie(Long ID) throws BaseException {
        Optional<Movie> movieOptional = this.repo.findOne(ID);
        return movieOptional.orElseThrow(() -> new BaseException("No item with given id"));
    }

    public void updateMovie(Movie movie) {
        this.repo.update(movie);
    }

    public void removeMovie(Long ID) {
        this.repo.delete(ID);
    }

    public Set<Movie> getMovies() {
        return StreamSupport.stream(this.repo.findAll().spliterator(), false)
                            .collect(Collectors.toSet());
    }

    public Set<Movie> filterMoviesByYear(int year) {
        return StreamSupport.stream(this.repo.findAll().spliterator(), false)
                            .filter(movie -> movie.getYear() == year)
                            .collect(Collectors.toSet());
    }
}
