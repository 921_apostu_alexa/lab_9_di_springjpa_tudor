package controller;

import model.Client;
import model.Movie;
import model.Rental;
import repository.IRepository;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RentalController {
    private IRepository<Long, Rental> repo;

    public RentalController(IRepository<Long, Rental> repo) {
        this.repo = repo;
    }

    public void rent(Client client, Movie movie) {
        Date now = new Date();
        Rental rental = new Rental(client, movie, now);
        this.repo.save(rental);
    }

    public Set<Rental> getRentals() {
        return StreamSupport
                .stream(this.repo.findAll().spliterator(), false)
                .collect(Collectors.toSet());
    }

    public Set<Rental> filterRentalsByClient(String clientID) {
        return StreamSupport
                .stream(this.repo.findAll().spliterator(), false)
                .filter(rental -> rental.getClient().getEmail().equals(clientID))
                .collect(Collectors.toSet());
    }

    public Set<Rental> filterRentalsByStartingDate(Date date) {
        return StreamSupport
                .stream(this.repo.findAll().spliterator(), false)
                .filter(rental -> rental.getRentDate().after(date))
                .collect(Collectors.toSet());
    }

    public Movie getMostRentedMovie() {
        return StreamSupport
                .stream(this.repo.findAll().spliterator(), false)
                .collect(Collectors.groupingBy(Rental::getMovie, Collectors.counting()))
                .entrySet()
                .stream()
                .max((r1, r2) -> r1.getValue() > r2.getValue() ? 1 : 0)
                .get()
                .getKey();
    }
}
